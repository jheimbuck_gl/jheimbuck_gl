## Weekly Priorities

This communicates to the 2 to 3 big rocks I am trying to accomplish this week usually in the style of OKRs with the heading being an objective and bullet points being key results to accomplish that. It will be updated weekly. 

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not 

# 2022-12-19
1. **N** - Publish recap / direction updates from Secrets Management Think Big
1. **N** - Move Draft of acting GMP blog into review

# 2022-12-05
1. **N** - Publish recap / direction updates from Secrets Management Think Big
1. **Y** - Update CI/CD Minute limits epic / direction to list iterations
1. **Y** - Get 15.7 release post / deprecation MRs into review or moved out

# 2022-11-28
1. **Y** - Update Verify OKRs for Key Product Meeting
1. **N** - Publish recap / direction updates from Secrets Management Think Big
1. **Y** - Moderate Office Hours for CI/CD Minutes Limits community contributions
1. **Y** - 15.8 Release Planning / 15.7 Release Post drafts

# 2022-11-14
1. **N** - Finalize OKR Issue mapping
1. **Y** - Prep/Present Ops Section for Monthly Kickoff call
1. **Y** - Merge Verify/PE MR for next week's Performance Indicator Meeting
1. **Y** - Moderate first ThinkBig for Secrets Management

# 2022-11-07
1. **Y** - Finalize Verify OKR Planning
    - Final issue creation pending
1. **Y** - Finalize list of cross stage Research with Erika
1. **N** - Move cross stage secrets research forward (Prep for Think Bigs and distribute materials)

# 2022-10-31
1. **N** - Finalize Verify OKR Planning
1. **Y** - Talent assessment tasks
1. **N** - Finalize list of cross stage Research with Erika
1. **Y** - Move cross stage secrets research forward (Prep for AMA)

# 2022-10-24 (Limited availability, at KubeCon NA)
1. **N** - Update OKR planning issue to tie to product OKRs
    - More widely socialize the [PE SUS proposal](https://gitlab.com/gitlab-com/Product/-/issues/4900) in Verify
1. **Y** - [Kubecon research](https://gitlab.com/gitlab-org/ux-research/-/issues/2126)
    - We had great conversations with customers on general topics and some good responses via survey and UX game though not as many as we wanted. Next steps are Think Big sessions the week of November 14th.

# 2022-10-17
1. **N** Update OKR planning issue to tie to product OKRs
    - More widely socialize the [PE SUS proposal](https://gitlab.com/gitlab-com/Product/-/issues/4900) in Verify
1. **Y** Record 15.7 refinement kickoff
1. **Y** Finalize Kubecon lightning talk images
1. **Y** Move [Split PE](https://gitlab.com/gitlab-org/verify-stage/-/issues/231) team issue forward in Product
    - Split from engineering re-org for fullstack engineers to simplify.

# 2022-10-10
1. **Y** - Submit [Lightning talk](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/7001) for Kubecon Booth
    - Images need finalized but otherwise ready.
2. **Y** - Sept Ops PI review
    - MR is in review
3. **Y** - 15.6 Kickoff Recording / 15.7 issue split planning
4. **Y** - Move OKR planning forward
    - PE Proposal is getting wider review, next steps are to tie Product OKRs to stage level

# 2022-10-03 (Short week F&F day on 10-07)
1. **Y** - Complete [+/- 10% exercise](https://gitlab.com/gitlab-com/Product/-/issues/4880) for PE
1. **Y** - Kickoff [Verify Q4 OKRs](https://gitlab.com/gitlab-com/Product/-/issues/4370)
1. **Y** - Catch-up on FY23Q4 / FY24 Discovery activities
1. **N** - Draft [Lightning talk](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/7001) for Kubecon Booth
1. **N** - Draft GMP Shadow Blog

# 2022-09-26
1. **N** - Complete +/- 10% exercise for PE
2. **Y** - 15.4 Direction updates, 15.5 release post drafts
3. **N** - Draft GMP Shadow Blog
4. **Y** - Update [Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/4891) for Kubecon

# 2022-09-19 (Short Week, shifted F&F day on 09-23)
1. **Y** - 15.6, 15.7, 15.8 planning
2. **Y** - Follow-up customer call about CI gaps
3. **Y** - PI Updates for Aug
4. **N** - Draft GMP Shadow Blog

# 2022-09-12
1. **Y** - PE and Verify 15.5 Kickoff todos
2. **Y** - Any logistics for CI Scaling after sync with Grzegorz
3. **N** - Meeting with Sarah re: portfolio planning for GMP Shadow
    * Turned into async discussion in the doc.
4. **N** - Follow-up customer call about CI gaps
    * Call is uploaded to Dovetail and PMs will be tagged this week on areas of interest

# 2022-09-05
1. **Y** - Update 15.6, 15.7 planning issues
2. **Y** - Get alignment on next steps for recent unexpected milestone issues
3. **Y** - Cover any issues for Dov and Jocelyn while they are at offsite/PTO

# 2022-08-29
1. **N** Update 15.6, 15.7 planning issues
1. **Y** Covering any tasks for Dov and Jocelyn
1. **N** - Figure out some next steps to drive Ultimate MAU (build and non build opportunities)

# 2022-08-22
1. **Y** Catchup on todos after PTO
1. **Y** Category Direction updates / review team updates
1. **N** 15.5, 15.6 and 15.7 planning updates
    1. 15.5 issue updated and refinement kickoff recorded. 15.6 and 15.7 planning TBD.
1. **Y** - Any PI review follow-ups

# 2022-08-01
1. **Y** - Kickoff Internship with Ashley
1. **Y** - 15.4 Planning and Kickoff Recording
1. **Y** - Draft Aug Performance Indicator MR

# 2022-07-25
1. **Y** - [Publish Verify Week blog](https://about.gitlab.com/blog/2022/07/28/verify-week-hackathon/)
1. **Y** - Publish [Coverage issue](https://gitlab.com/gitlab-com/Product/-/issues/4375) for Aug PTO
1. **N** - Direction Updates, PI follow-ups and Q3 OKRs
    - Direction Update merged
    - Q3 OKRs entered into Ally
    - PI follow-ups pending

# 2022-07-18
1. **N** - [Publish Verify Week blog](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13678)
    - In editorial review. Links/videos need to be added.
1. **N** - Publish Coverage issue for Aug PTO
    - Waiting on review of one task assignment otherwise ready to go.
1. **N** - Update External SCM support Opportunity Canvas from last week's feedback
    - Received additional feedback from a TAM about the business case.
1. **Y** - [Close Verify Borrow request](https://gitlab.com/gitlab-com/Product/-/issues/4467)

# 2022-06-20 (Shortened week)
1. **Y** - Finish Opportunity Review to move associated epics forward in Q2/Q3
1. **N** - Create borrow request for S2 bug squash
    - We decided there were not enough S2 bugs that were SUS issues or would help move CI Maturity forward to do this.

# 2022-06-13
1. **N** - Finish Opportunity Review to move associated epics forward in Q2/Q3
1. **Y** - [15.3](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/104), [15.4](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/107), 15.5 planning 

# 2022-06-06
1. **Y** - Finish [15.2 planning](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/101) for kickoff call next week.
1. **N** - Finish Opportunity Review to move associated epics forward in Q2/Q3
1. **N** - [15.3](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/104), [15.4](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/107) planning 

# 2022-05-30
1. **N** - Finish Opportunity Review to move associated epics forward in Q2/Q3
1. **N** - [15.3](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/104), [15.4](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/107) planning 

# 2022-05-23
1. **N** - Finish Opportunity Review to move associated epics forward in Q2/Q3
1. **Y** - Publish [video walkthrough](https://www.youtube.com/watch?v=tVGT6vaeX4U) of Maturity plan
1. **N** - [15.3](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/104), [15.4](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/107) planning 

# 2022-05-16
1. **N** - Draft an [investment case](https://gitlab.com/gitlab-com/Product/-/issues/4178) for May review
    - The investment case has turned into an [opportunity review](https://gitlab.com/gitlab-com/Product/-/issues/3936).
1. **N** - Prep for FY23 Maturity updates

# 2022-05-09
1. **N** - Draft an [investment case](https://gitlab.com/gitlab-com/Product/-/issues/4178) for May review
1. **N** - Prep for FY23 Maturity updates

# 2022-05-02
1. **N** - Pick an [investment case](https://gitlab.com/gitlab-com/Product/-/issues/4041) for April and Draft it for GMP review
1. **N** - Prep for FY23 Maturity updates
    - ~~Create a deck walking back from proposed dates with todos for research, implementation, etc.~~
    - Record and publish walkthrough of the deck
    - Update maturity dates as needed

# 2022-04-18 (Short week for delayed F&F day)
1. **N** - Pick an [investment case](https://gitlab.com/gitlab-com/Product/-/issues/4041) for April and Draft it for GMP review
1. **N** - Prep for FY23 Maturity updates
    - Create a deck walking back from proposed dates with todos for research, implementation, etc.
    - Record and publish walkthrough of the deck
    - Update maturity dates as needed

# 2022-04-11
1. **N** - Pick an [investment case](https://gitlab.com/gitlab-com/Product/-/issues/4041) for April and Draft it for GMP review
1. **Y** - Capacity planning for upcoming milestones
    - [15.0](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/93), [15.1](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/98), [15.2](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/101)

# 2022-04-04
1. **Y** Three Customer conversations / follow-ups
    - Two calls uploaded to Dovetail ([1](https://dovetailapp.com/data/6CXAafOOdcW65qZ4j98n0g), [2](https://dovetailapp.com/data/1D6uMZjN9jQKF7gSbCouzn))
1. **N** - Pick an [investment case](https://gitlab.com/gitlab-com/Product/-/issues/4041) for April and Draft it for GMP review
1. **Y** - Catch-up on direction updates

# 2022-03-28
1. **Y** - Catch up on todos and pings
1. **N** - Move shared runner MAU canvas forward / ready for review
    - There's a survey fielding this week to gather more information.
1. **Y** - Close out Pipipeline Execution PI [discussion issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/95)

# 2022-03-14
1. **Y** - Populate [coverage issue](https://gitlab.com/gitlab-com/Product/-/issues/3739) for March PTO (2022-03-21 - 2022-03-25) and wrap-up any pending todos for next week.
1. **Y** - Move External SCM and GitLab CI research forward
    - Two interviews this week
    - Create insights from interviews to date
1. **Y** - SKO on Thursday
    - Two presentations + two panels

# 2022-03-07
1. **Y** - SKO presentation prep
    - Content complete, some tweaks to go, run through completed and ready for this week!
1. **N** - Move External SCM and GitLab CI research forward
    - One interview no showed and the other was not actually a fit. I missed on the screener review.
1. **Y** - Normal Release Cadence work
1. **N** - Populate [coverage issue](https://gitlab.com/gitlab-com/Product/-/issues/3739) for March PTO (2022-03-21 - 2022-03-25)

# 2022-02-28
1. **Y** - Finish SKO deck
1. **Y** - Get some reporting for CI Minutes Limits next week
    - [Weekly Results Update](https://gitlab.com/groups/gitlab-org/-/epics/5653#note_857825624)
1. **Y** - Research tasks
    - First MT improvements interview complete, 5 interviews scheduled for External SCM with GitLab CI
1. **N** - Normal release cadence tasks (Direction updates, release posts, etc.)

# 2022-02-21 (Short week for holiday and F&F day)
1. **Y** - Catch up from [time off](https://gitlab.com/gitlab-com/Product/-/issues/3739)
1. **N** - Move CI Minutes Limits forward towards getting a results entry next week
    - Issue for metrics collection [created](https://gitlab.com/gitlab-org/gitlab/-/issues/353803) to capture % of projects with a cost factor. Scheduled for %14.10 for now.
1. **N** - Finish SKO deck
    - About half done, on track for this week.

# 2022-02-14 (async / time off)
1. **N** - Finish populating SKO deck
    - Outline done, will wrap up extra slides and talk track next.
1. **Y** - Wrap things up for [time off](https://gitlab.com/gitlab-com/Product/-/issues/3739)
    - 14.8 Release post last minute stuff
    - PI updates
    - 14.9 planning
    - CI minutes iteration plan in the epic

# 2022-02-07
1. **Y** - Move Q1 research efforts forward by populating discussion guide slides for upcoming research and booking interviews
    - Invitation email sent, 1 booked.
    - Slides updated for MT research
1. **Y** - Create direction updates from RICE prioritization exercise / FY23 look forward
    - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/91)
1. **Y** - Start discussion with team about changing GMAU
    - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/95)
1. **Y** - Create SKO deck
    - Created with outline, slides need to be filled in completely w/ talk track.

# 2022-01-31
1. **Y** - Finalize RICE Prioritization issue to collaborate on FY23 vision with the team
    - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/91)
1. **N** - Move Q1 research efforts forward by populating discussion guide slides for upcoming research and booking interviews for next week
	- Merge Trains
	- Using GitLab CI with external SCM
1. **N** - Start discussion with team about changing GMAU
1. **Y** - Hydrate coverage issues for Feb and Mar

# 2022-01-24
1. **N** - Finalize RICE Prioritization issue to collaborate on FY23 vision with the team
    - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/91) - due date extended to 2022-01-31
1. **Y** - FY23Q1 Planning Tasks
    - Finalize Q1 OKRs w/ Quad and enter into Ally
    - Hydrate 14.9, 14.10, 15.0 planning issues
    - Record 14.9 Refinement Kickoff
1. **N** - Finalize Opportunity Canvas for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) using learnings from past research analysis 
1. **Y** - Plan some Time Off in Feb & March
    - [Feb issue](https://gitlab.com/gitlab-com/Product/-/issues/3739)
    - [Mar issue](https://gitlab.com/gitlab-com/Product/-/issues/3739)

# 2022-01-17
1. **Y** - RICE Prioritization issue to collaboa\rate on FY23 vision with the team
    - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/91)
1. **Y** - Normal PM Cadence work (PI update prep, Release Post, 14.8 Kickoff)
    - [Performance Indicator MR](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/merge_requests/303)
    - [14.8 Planning issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/83)/[PE Kickoff Recording](https://youtu.be/pGSSh6bvzIA)
1. **N** - Finalize Opportunity Canvas for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) using learnings from past research analysis 

# 2022-01-10
1. **N** - Finalize Opportunity Canvas for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) using learnings from past research analysis 
1. **N** - Move Opportunity Canvas lite for [CI for External SCM](https://gitlab.com/gitlab-com/Product/-/issues/2314) forward with iterations on all sections.
1. **Y** - Organize the existing and possible work between CI Minutes efforts and Community Contribution work into more manageable issue or epic
    - Captured in [this comment](https://gitlab.com/gitlab-com-top-initiatives/free-saas/-/issues/39#note_810014697)

# 2022-01-03
1. **Y** - Finish [competitive landscape research issue](https://gitlab.com/gitlab-com/Product/-/issues/3336) forward by merging `devops_tools` MR
1. **N** - Finalize Opportunity Canvas for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) using learnings from past research analysis 
1. **N** - Move Opportunity Canvas lite for [CI for External SCM](https://gitlab.com/gitlab-com/Product/-/issues/2314) forward with iterations on all sections.
1. **N** - Record reflective video from CEO Shadow weeks
